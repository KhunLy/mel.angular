import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PokeDetailsModel } from '../models/poke-details.model';
import { PokeResultModel } from '../models/poke-result.model';

@Injectable({providedIn: 'root'})
export class PokeService {

  constructor(
    private client: HttpClient
  ) { }

  getAll() : Observable<PokeResultModel>{
    return this.client.get<PokeResultModel>("https://pokeapi.co/api/v2/pokemon");
  }

  getDetails(url) : Observable<PokeDetailsModel>{
    return this.client.get<PokeDetailsModel>(url);
  }
}
