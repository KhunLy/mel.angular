export interface PokeResultModel {
    count: number;
    next: string;
    previuos: string;
    results: Pokemon[];
}

export interface Pokemon {
    name: string,
    url: string
}