import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';


@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  someString: string;
  someNumber: number;
  someDate: Date;
  someBoolean: boolean;
  
  constructor() { }

  ngOnInit(): void {
    this.someString = "AnGular";
    this.someNumber = 3.45697;
    this.someDate = new Date();
    this.someBoolean = true;
    
    setTimeout(() => {
      this.someString  = "Autre Chose";
    }, 3000);
  }

}
