import { Component, Input, OnInit } from '@angular/core';
import { PokeDetailsModel } from 'src/app/models/poke-details.model';

@Component({
  selector: 'app-demo3-details',
  templateUrl: './demo3-details.component.html',
  styleUrls: ['./demo3-details.component.scss']
})
export class Demo3DetailsComponent implements OnInit {

  @Input()
  model: PokeDetailsModel

  constructor() { }

  ngOnInit(): void {
  }

}
