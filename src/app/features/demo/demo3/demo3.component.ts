import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PokeDetailsModel } from 'src/app/models/poke-details.model';
import { PokeResultModel } from 'src/app/models/poke-result.model';
import { PokeService } from 'src/app/services/poke.service';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {

  model: PokeResultModel;

  details: PokeDetailsModel;

  constructor(
    private pokeService : PokeService
  ) {
  }

  ngOnInit(): void {
    this.pokeService.getAll().subscribe(
      data => this.model = data,
      error => {},
      () => {}
    );
  }

  search(url: string) {
    console.log(url);
    this.pokeService.getDetails(url).subscribe(data => {
      this.details = data;
    });
    //faire une requete
  }

}
