import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {

  compt: number

  constructor() { }

  ngOnInit(): void {

    this.compt = 0;
  }

  incrase() {
    if(this.compt >= 9) return;
    this.compt++;
  }

  decrease() {
    if(this.compt <= 0) return;
    this.compt--;
  }

}
